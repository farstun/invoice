<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions the admin area.
 *
 * @link       https://farstun.com
 * @since      1.0.0
 *
 * @package    Invoice
 * @subpackage Invoice/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define admin-specific hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Invoice
 * @subpackage Invoice/includes
 * @author     Jonas Eriksson <jonas@farstun.com>
 */
class Invoice {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Invoice_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies and set the hooks for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'INVOICE_VERSION' ) ) {
			$this->version = INVOICE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'invoice';

		$this->load_dependencies();
		$this->define_admin_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Invoice_Loader. Orchestrates the hooks of the plugin.
	 * - Invoice_Admin. Defines all hooks for the admin area.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-invoice-loader.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-invoice-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-invoice-settings.php';


		$this->loader = new Invoice_Loader();
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Invoice_Admin(
			$this->get_plugin_name(),
			$this->get_version(),
			Invoice_Settings::$settings_page,
			Invoice_Settings::$option_name
		);

		$this->loader->add_filter( 'plugin_action_links', $plugin_admin, 'add_settings_link', 10, 2 );

		$this->loader->add_action('admin_menu', $plugin_admin, 'add_invoice_menu');

		// https://www.advancedcustomfields.com/resources/register-fields-via-php/
		// TODO: add_action('acf/init', 'my_acf_add_local_field_groups');

		$this->loader->add_action( 'init', $plugin_admin, 'register_post_types' );
		$this->loader->add_action( 'acf/init', $plugin_admin, 'register_custom_acf_fields' );
		$this->loader->add_action( 'pre_get_posts', $plugin_admin, 'custom_orderby' );

		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'check_required_plugins' );

		// Prevent access to Customer and Invoice post types for unauthorized users.
		$this->loader->add_action( 'template_redirect', $plugin_admin, 'redirect_unauthenticated' );

		$this->loader->add_filter( 'single_template', $plugin_admin, 'load_invoice_template' );		

		// Custom columns on Customer
		$this->loader->add_action( 'manage_farstun_customer_posts_custom_column', $plugin_admin, 'custom_customer_column', 10, 2 );
		$this->loader->add_filter( 'manage_farstun_customer_posts_columns', $plugin_admin, 'set_custom_edit_customer_columns' );
		$this->loader->add_filter( 'manage_edit-farstun_customer_sortable_columns', $plugin_admin, 'set_customer_sortable_columns' );

		// Custom top link on Invoice
		$this->loader->add_action( 'views_edit-farstun_invoice', $plugin_admin, 'add_top_link' );

		// Custom content to tablenav
		$this->loader->add_action( 'manage_posts_extra_tablenav', $plugin_admin, 'add_invoice_total' );

		// Custom columns on Invoice
		$this->loader->add_action( 'manage_farstun_invoice_posts_custom_column', $plugin_admin, 'custom_invoice_column', 10, 2 );
		$this->loader->add_filter( 'manage_farstun_invoice_posts_columns', $plugin_admin, 'set_custom_edit_invoice_columns' );
		$this->loader->add_filter( 'manage_edit-farstun_invoice_sortable_columns', $plugin_admin, 'set_invoice_sortable_columns' );
		
		// Remove Bulk actions
		$this->loader->add_filter ( 'bulk_actions-edit-farstun_customer', $plugin_admin, 'remove_bulk_actions' );
		$this->loader->add_filter ( 'bulk_actions-edit-farstun_invoice', $plugin_admin, 'remove_bulk_actions' );

		// Remove Date filter
		$this->loader->add_filter ( 'months_dropdown_results', $plugin_admin, 'remove_date_filter', 10, 2 );

		// Remove Quick Edit
		$this->loader->add_filter ( 'post_row_actions', $plugin_admin, 'remove_quick_edit', 10, 2 );

		// Add Email link
		$this->loader->add_filter ( 'post_row_actions', $plugin_admin, 'add_email_link', 10, 2 );

		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'update_invoice_fields' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'hide_invoice_title' );

		// Settings page
		$this->loader->add_action( 'admin_menu', 'Invoice_Settings', 'add_options_page' );
		$this->loader->add_action( 'admin_init', 'Invoice_Settings', 'setup_settings' );
		$this->loader->add_action( 'update_option_invoice_option', 'Invoice_Settings', 'options_updated' );
		$this->loader->add_action('load-edit.php', 'Invoice_Settings', 'notice_missing_settings' );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_admin, 'enqueue_cpt_styles' );

		$this->loader->add_filter( 'update_plugins_gitlab.com', $plugin_admin, 'check_for_updates', 10, 3 );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Invoice_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
