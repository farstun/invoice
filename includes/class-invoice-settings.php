<?php

/**
 * Used for plugin settings options.
 *
 * @link       https://farstun.com
 * @since      1.0.0
 *
 * @package    Invoice
 * @subpackage Invoice/includes
 */

/**
 * Used for plugin settings options.
 *
 * This class defines all code necessary to create settings options.
 *
 * @since      1.0.0
 * @package    Invoice
 * @subpackage Invoice/includes
 * @author     Jonas Eriksson <jonas@farstun.com>
 */
class Invoice_Settings {
	public static $plugin_name = 'invoice';
	public static $settings_page =  'invoice-settings';
	public static $option_name = 'invoice_option';

	/**
	 * Holds the values to be used in the fields callbacks
	 */
	public static $options;

	/**
	 * Add options page
	 */
	public static function add_options_page() {
		// This page will be under custom top level menu page
		add_submenu_page(
			'invoice.php',
			'Farstun Invoice Settings',
			'Settings',
			'manage_options',
			self::$settings_page,
			array( 'Invoice_Settings', 'create_options_page' )
		);
	}

	/**
	 * Options page callback
	 */
	public static function create_options_page() {
		// Set class property
		self::$options = get_option( self::$option_name );
		?>
		<div class="wrap">
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<?php settings_errors(); ?>
			<form method="post" action="options.php">
			<?php
				// This prints out all hidden setting fields
				settings_fields( self::$plugin_name . '_option_group' );
				do_settings_sections( self::$settings_page );
				submit_button();
			?>
			</form>
		</div>
		<?php
	}

	public static function missing_settings_warning() {
		?>
		<div class="notice notice-warning is-dismissible">
			<p>
				Missing settings. Please visit 
				<a href="<?php echo esc_url( admin_url( 'admin.php' ) ) . '?page=' . self::$settings_page; ?>">Settings</a>
				and enter required data.
			</p>
		</div>
		<?php
	}

	public static function notice_missing_settings() {
		// Ensure there settings are entered if we are to edit invoice
		if ( isset($_GET['post_type']) && 'farstun_invoice' === $_GET['post_type'] ) {
			self::$options = get_option( self::$option_name );
			if ( ! isset( self::$options['name'] ) || empty( self::$options['name'] ) ) {
				add_action( 'admin_notices', array('Invoice_Settings', 'missing_settings_warning' ) );
			}
		}
	}

	/**
	 * Hook into options page after save.
	 */
	public static function options_updated() {
		add_settings_error( 'invoice_option', self::$option_name, 'Settings updated.', 'success' );
	}

	/**
	 * Register and add settings
	 */
	public static function setup_settings() {
		register_setting(
			self::$plugin_name . '_option_group', // Option group
			self::$option_name, // Option name
			array( 'Invoice_Settings', 'sanitize' ) // Sanitize
		);

		/**
		 * Company section
		 */

		add_settings_section(
			'setting_section_company', // ID
			'Company', // Title
			array( 'Invoice_Settings', 'print_company_section_info' ), // Callback
			self::$settings_page // Page
		);

		add_settings_field(
			'logotype', // ID
			'Logotype', // Title
			array( 'Invoice_Settings', 'logotype_callback' ), // Callback
			self::$settings_page, // Page
			'setting_section_company' // Section
		);

		add_settings_field(
			'name',
			'Name',
			array( 'Invoice_Settings', 'name_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		add_settings_field(
			'org_number',
			'Organization Number',
			array( 'Invoice_Settings', 'org_number_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		add_settings_field(
			'address',
			'Address',
			array( 'Invoice_Settings', 'address_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		add_settings_field(
			'email',
			'Email',
			array( 'Invoice_Settings', 'email_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		add_settings_field(
			'homepage',
			'Homepage (URL)',
			array( 'Invoice_Settings', 'homepage_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		add_settings_field(
			'vat_registered',
			'VAT Registered',
			array( 'Invoice_Settings', 'vat_registered_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		add_settings_field(
			'f_approved',
			'F-skatt Approved',
			array( 'Invoice_Settings', 'f_approved_callback' ),
			self::$settings_page,
			'setting_section_company'
		);

		/**
		 * Invoice section
		 */

		add_settings_section(
			'setting_section_invoice', // ID
			'Invoice', // Title
			array( 'Invoice_Settings', 'print_invoice_section_info' ), // Callback
			self::$settings_page // Page
		);

		add_settings_field(
			'account_type', // ID
			'Account Type', // Title
			array( 'Invoice_Settings', 'account_type_callback' ), // Callback
			self::$settings_page, // Page
			'setting_section_invoice' // Section
		);

		add_settings_field(
			'account_number',
			'Account Number',
			array( 'Invoice_Settings', 'account_number_callback' ),
			self::$settings_page,
			'setting_section_invoice'
		);

		add_settings_field(
			'due_days',
			'Default Due Days',
			array( 'Invoice_Settings', 'due_days_callback' ),
			self::$settings_page,
			'setting_section_invoice'
		);

		add_settings_field(
			'invoice_number',
			'Invoice Number',
			array( 'Invoice_Settings', 'invoice_number_callback' ),
			self::$settings_page,
			'setting_section_invoice'
		);
	}

	/**
	 * Print the Company Section text
	 */
	public static function print_company_section_info() {
		print 'General company information. Note: all information is visible on the invoice.';
	}

	/**
	 * Print the Invoice Section text
	 */
	public static function print_invoice_section_info() {
		print 'General invoice settings.';
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public static function sanitize( $input ) {
		$new_input = array();

		if ( isset( $input['logotype'] ) )
			$new_input['logotype'] = esc_url_raw( $input['logotype'] );

		if ( isset( $input['name'] ) )
			$new_input['name'] = sanitize_text_field( $input['name'] );

		if ( isset( $input['org_number'] ) )
			$new_input['org_number'] = sanitize_text_field( $input['org_number'] );

		if ( isset( $input['address'] ) )
			$new_input['address'] = sanitize_textarea_field( $input['address'] );

		if ( isset( $input['email'] ) )
			$new_input['email'] = sanitize_email( $input['email'] );

		if ( isset( $input['homepage'] ) )
			$new_input['homepage'] = rtrim( esc_url_raw( $input['homepage'] ), "/" );

		if ( isset( $input['vat_registered'] ) )
			$new_input['vat_registered'] = $input['vat_registered'] === "1" ? "1" : "0";

		if ( isset( $input['f_approved'] ) )
			$new_input['f_approved'] = $input['f_approved'] === "1" ? "1" : "0";


		if ( isset( $input['account_type'] ) )
			$new_input['account_type'] = sanitize_text_field( $input['account_type'] );

		if ( isset( $input['account_number'] ) )
			$new_input['account_number'] = sanitize_text_field( $input['account_number'] );

		if ( isset( $input['due_days'] ) )
			$new_input['due_days'] = sanitize_text_field( $input['due_days'] );

		if ( isset( $input['invoice_number'] ) )
			$new_input['invoice_number'] = max(intval( sanitize_text_field( $input['invoice_number'] ) ), 1);

		return $new_input;
	}

	/**
	 * Get the settings option array and print one of its values
	 */

	public static function logotype_callback() {
		$logotype_url = isset( self::$options['logotype'] ) ? esc_attr( self::$options['logotype']) : '';
		printf(
			'<input type="hidden" id="logotype" name="%s[logotype]" value="%s" />' .
			'<img id="logotype-preview" class="logotype-preview" src="%s" />' .
			'<input type="button" id="upload_image_button" class="button" value="Select Image" />',
			self::$option_name,
			$logotype_url,
			$logotype_url
		);
	}

	public static function name_callback() {
		printf(
			'<input type="text" id="name" name="%s[name]" value="%s" required />',
			self::$option_name,
			isset( self::$options['name'] ) ? esc_attr( self::$options['name']) : ''
		);
	}

	public static function org_number_callback() {
		printf(
			'<input type="text" id="org_number" name="%s[org_number]" value="%s" placeholder="XXXXXX-XXXX" required />',
			self::$option_name,
			isset( self::$options['org_number'] ) ? esc_attr( self::$options['org_number']) : ''
		);
	}

	public static function address_callback() {
		printf(
			'<textarea id="address" name="%s[address]" rows="4" required>%s</textarea><p class="description">%s</p>',
			self::$option_name,
			isset( self::$options['address'] ) ? esc_attr( self::$options['address']) : '',
			'Exclude company name.'
		);
	}

	public static function email_callback() {
		printf(
			'<input type="email" id="email" name="%s[email]" value="%s" required />',
			self::$option_name,
			isset( self::$options['email'] ) ? esc_attr( self::$options['email']) : ''
		);
	}

	public static function homepage_callback() {
		printf(
			'<input type="url" id="homepage" name="%s[homepage]" value="%s" class="regular-text" placeholder="https://..." required />',
			self::$option_name,
			isset( self::$options['homepage'] ) ? esc_attr( self::$options['homepage']) : ''
		);
	}

	public static function vat_registered_callback() {
		printf(
			'<input type="checkbox" id="vat_registered" name="%s[vat_registered]" value="1" %s />',
			self::$option_name,
			isset( self::$options['vat_registered'] ) && esc_attr( self::$options['vat_registered']) === '1' ? 'checked' : ''
		);
	}

	public static function f_approved_callback() {
		printf(
			'<input type="checkbox" id="f_approved" name="%s[f_approved]" value="1" %s />',
			self::$option_name,
			isset( self::$options['f_approved'] ) && esc_attr( self::$options['f_approved']) === '1' ? 'checked' : ''
		);
	}

	public static function account_type_callback() {
		$selected_account_type = isset( self::$options['account_type'] ) ? esc_attr( self::$options['account_type']) : '';
		printf(
			'<select id="account_type" name="%s[account_type]">%s</select>',
			self::$option_name,
			array_reduce(ACCOUNT_TYPES, function($res, $item) use($selected_account_type)
			{
				return sprintf('%s<option value="%s" %s>%s</option>', $res, $item, $item === $selected_account_type ? 'selected' : '', $item);
			}, '')
		);
	}

	public static function account_number_callback() {
		printf(
			'<input type="text" id="account_number" name="%s[account_number]" value="%s" required />',
			self::$option_name,
			isset( self::$options['account_number'] ) ? esc_attr( self::$options['account_number']) : ''
		);
	}

	public static function due_days_callback() {
		printf(
			'<input type="number" id="due_days" name="%s[due_days]" value="%s" required />',
			self::$option_name,
			isset( self::$options['due_days'] ) ? esc_attr( self::$options['due_days']) : '30'
		);
	}

	public static function invoice_number_callback() {
		printf(
			'<input type="number" id="invoice_number" name="%s[invoice_number]" value="%s" required /><p class="description">%s</p>',
			self::$option_name,
			isset( self::$options['invoice_number'] ) ? esc_attr( self::$options['invoice_number']) : '1',
			"The invoice number is increased when an invoice is created and is used for automatic invoice number.<br />Avoid manually changing this number."
		);
	}
}
