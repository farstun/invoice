<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://farstun.com
 * @since      1.0.0
 *
 * @package    Invoice
 * @subpackage Invoice/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Invoice
 * @subpackage Invoice/includes
 * @author     Jonas Eriksson <jonas@farstun.com>
 */
class Invoice_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		// https://developer.wordpress.org/reference/functions/register_post_type/#flushing-rewrite-on-activation
		flush_rewrite_rules();
	}

}
