# Farstun Invoice

WordPress plugin to create and manage invoices.

## Description

WordPress plugin to create and manage invoices. Requires the Advanced Custom Fields PRO plugin.

The plugin is based on baserat [WordPress Plugin Boilerplate skeleton](https://github.com/DevinVinson/WordPress-Plugin-Boilerplate),
as recommended by [Plugin Basics / Best Practices / Boilerplate Starting Points](https://developer.wordpress.org/plugins/plugin-basics/best-practices/#boilerplate-starting-points).

Farstun Invoice is intended as an utility to send invoices. Hence there are only admin features, i.e. NO public-facing functionality.

A few notes about the sections above:

*   "Tags" is a comma separated list of tags that apply to the plugin
*   "Requires at least" is the lowest version that the plugin will work on
*   "Tested up to" is the highest version that you've *successfully used to test the plugin*. Note that it might work on
higher versions... this is just the highest one you've verified.
*   Stable tag should indicate the Subversion "tag" of the latest stable version, or "trunk," if you use `/trunk/` for
stable.

    Note that the `readme.txt` of the stable tag is the one that is considered the defining one for the plugin, so
if the `/trunk/readme.txt` file says that the stable tag is `4.3`, then it is `/tags/4.3/readme.txt` that'll be used
for displaying information about the plugin.  In this situation, the only thing considered from the trunk `readme.txt`
is the stable tag pointer.  Thus, if you develop in trunk, you can update the trunk `readme.txt` to reflect changes in
your in-development version, without having that information incorrectly disclosed about the current stable version
that lacks those changes -- as long as the trunk's `readme.txt` points to the correct stable tag.

    If no stable tag is provided, it is assumed that trunk is stable, but you should specify "trunk" if that's where
you put the stable version, in order to eliminate any doubt.

## Internationalization

There is currently no i18n support, use [WordPress Plugin Boilerplate skeleton](https://github.com/DevinVinson/WordPress-Plugin-Boilerplate) as template for future support.

## Installation

1. Upload `invoice` folder with content to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

## Screenshots

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

## Changelog

### 1.1.1
* Display total invoice amount according to filter

### 1.1.0
* Display unpaid as top link with total amount

### 1.0.0
* Initial release.

## TODO

* Customer: Add search support for all listed columns
* Invoice: Add search support for all listed columns
* Invoid: Filter on customer, add customer select using restrict_manage_posts action
* Comment and clean up code

### Custom search

* [How to include custom fields in wordpress search](https://wordpress.stackexchange.com/questions/414404/how-to-include-custom-fields-in-wordpress-search)
* [Search by custom field](https://wordpress.org/support/topic/search-by-custom-field-2/)

### Update private plugin

Farstun Invoice is publicly accessible in order to enable plugin update. This is currently not implemented. For implementation guide, refer to

* [Custom WordPress Plug-in Repository With Automatic Updates](https://www.jasom.net/custom-wordpress-plugin-repository/)
* [How to Configure Self-Hosted Updates for Your Private Plugins](https://rudrastyh.com/wordpress/self-hosted-plugin-update.html)
* [Updates for a private plugin?](https://wordpress.stackexchange.com/questions/13/updates-for-a-private-plugin)
* [Plugin Update Checker](https://github.com/YahnisElsts/plugin-update-checker)

### Required data

Ensure all reqired data is present; [vad-ska-en-faktura-innehalla](https://www.verksamt.se/om-webbplatsen/tillganglighet/bildtexter/vad-ska-en-faktura-innehalla).

Currently VAT is missing?

## Future improvements

* Support credit invoice; click button to credit invoice
* Create PDF using plugin. This is future feature due to CSS (grid) restictions.
* Send invoice mail using plugin. Depends on PDF creation.
* Add support for [SIE files](https://sie.se/wp-content/uploads/2020/05/SIE_filformat_ver_4B_ENGLISH.pdf).
* Support for "dröjesmålsränta"
* Option to add free-text in settings that is include on every invoice
* Change "timmar" to "h"
* Obfuscate url's and add check if link has been opened