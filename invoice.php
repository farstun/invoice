<?php

/**
 * @link              https://farstun.com
 * @since             1.0.0
 * @package           Invoice
 *
 * @wordpress-plugin
 * Plugin Name:       Farstun Invoice
 * Plugin URI:        https://farstun.com
 * Update URI:        https://gitlab.com/farstun/invoice/-/raw/main/plugin-info.json
 * Description:       Create and manage invoices. Requires the Advanced Custom Fields PRO plugin.
 * Version:           1.1.1
 * Author:            Jonas Eriksson
 * Author URI:        https://farstun.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       invoice
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'INVOICE_VERSION', '1.1.1' );

if ( ! defined( 'INVOICE_PLUGIN_BASE' ) ) {
	// in main plugin file 
	define( 'INVOICE_PLUGIN_BASE', plugin_basename( __FILE__ ) );
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-invoice-activator.php
 */
function activate_invoice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-invoice-activator.php';
	Invoice_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-invoice-deactivator.php
 */
function deactivate_invoice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-invoice-deactivator.php';
	Invoice_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_invoice' );
register_deactivation_hook( __FILE__, 'deactivate_invoice' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-invoice.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_invoice() {

	$plugin = new Invoice();
	$plugin->run();

}
run_invoice();
