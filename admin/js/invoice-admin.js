jQuery(document).ready(function($) {
	'use strict';

    const due_days = invoice_options ? invoice_options.due_days : undefined;

	let custom_uploader;
    $('#upload_image_button').click((e) => {
        e.preventDefault();

		// If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        // Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });

		// When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', () => {
            const attachment = custom_uploader.state().get('selection').first().toJSON();
            $('#logotype').val(attachment.url);
			$('#logotype-preview').attr('src', attachment.url);
        });

		// Open the uploader dialog
        custom_uploader.open();
    });

    if (due_days) {
        const invoiceDate = acf.getField('field_6594a23554281');
        invoiceDate.on('change', (e) => {
            const newDate = invoiceDate.val();
            if (newDate !== '') {
                // Always on form Ymd, insert '-' to force format Y-m-d
                let chars = [...newDate];
                chars.splice(4, 0, '-');
                chars.splice(7, 0, '-');

                const selectedDate = new Date(`${chars.join('')}T00:00:00`);
                selectedDate.setDate(selectedDate.getDate() + parseInt(due_days));

                const invoiceDueDate = $('#acf-field_6594a2e454282').siblings('.hasDatepicker');
                invoiceDueDate.datepicker('setDate', selectedDate);
                invoiceDueDate.trigger('change');
            }
        });
    }
});
