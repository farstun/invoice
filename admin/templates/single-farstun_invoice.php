<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <?php wp_body_open(); ?>
        <?php if( have_posts() ): ?>
            <?php while ( have_posts()): the_post(); ?>
            <?php
                $customer = get_field('customer');
                $options = get_option( 'invoice_option' );

                $items = get_field('items');
                if ( empty( $items ) ) {
                    $items = [];
                }

                // Create array of all item price with and without VAT, grouped by VAT
                $items_vat = [
                    '0' => ['netto' => 0, 'vat' => 0],
                    '6' => ['netto' => 0, 'vat' => 0],
                    '12' => ['netto' => 0, 'vat' => 0],
                    '25' => ['netto' => 0, 'vat' => 0],
                ];
                foreach ($items as $item) {
                    $item_netto_total = (int) $item['number'] * (int) $item['price'];
                    $items_vat[$item['vat']]['netto'] += $item_netto_total;
                    $items_vat[$item['vat']]['vat'] += $item_netto_total * (int) $item['vat'] * 0.01;;
                }
                $items_vat = array_filter($items_vat, function($value) {
                    return $value['netto'] > 0;
                });

                // Calculate total invoice amount including VAT
                $invoice_total = get_post_meta( get_the_ID(), 'total_amount', true );
                $invoice_total = number_format( empty( $invoice_total ) ? 0 : $invoice_total, 2, ',', ' ' );
            ?>
                <article class="invoice">
                    <header class="invoice__header">
                        <?php if ( $options['logotype'] ): ?>
                            <img src="<?php echo $options['logotype']; ?>" class="logotype" />
                        <?php endif ?>
                        <h1>Faktura</h1>
                    </header>

                    <section class="invoice__content">
                        <div class="customer-address">
                            <strong><?php echo $customer->post_title; ?></strong>
                            <address>
                                <?php echo nl2br( get_field('address', $customer->ID) ); ?>
                            </address>
                        </div>

                        <div class="invoice-summary">
                            <h3>Att betala</h3>
                            <div><strong class="x-large"><?php echo $invoice_total; ?> SEK</strong> (inkl moms)</div>
                            <ul>
                                <li>
                                    <strong>Förfallodatum</strong>
                                    <span><?php the_field('due_date'); ?></span>
                                </li>
                                <li>
                                    <strong><?php echo $options['account_type']; ?></strong>
                                    <span><?php echo $options['account_number']; ?></span>
                                </li>
                                <li>
                                    <strong>Referensnr.</strong>
                                    <span><?php the_title(); ?></span>
                                </li>
                            </ul>
                        </div>

                        <ul class="invoice-meta">
                            <li>
                                <strong>Fakturanummer</strong>
                                <span><?php the_title(); ?></span>
                            </li>
                            <li>
                                <strong>Utställare</strong>
                                <span><?php echo $options['name']; ?></span>
                            </li>
                            <li>
                                <strong>Mottagare</strong>
                                <span><?php echo $customer->post_title; ?></span>
                            </li>
                            <li>
                                <strong>Fakturadatum</strong>
                                <span><?php the_field('invoice_date'); ?></span>
                            </li>
                        </ul>

                        <table class="invoice-details">
                            <thead>
                                <tr>
                                    <th colspan="3">Pris exkl. moms</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ( $items as $item ) {
                                        $title = $item['title'];
                                        $description = $item['description'];
                                        $number = (int) $item['number'];
                                        $unit = $item['unit'];
                                        $price = (int) $item['price'];
                                        $vat = (int) $item['vat'];
                                        $item_total = $number * $price;
                                        if ( 'timme' === $unit && $number !== 1) {
                                            $unit = 'timmar';
                                        }
                                        ?>
                                        <tr>
                                            <td class="invoice-details__cell description">
                                                <strong><?php echo $title; ?></strong><br />
                                                <?php echo $description; ?>
                                            </td>
                                            <td class="invoice-details__cell amount">
                                                <?php printf('%s %s x %s', $number, $unit, number_format( $price, 2, ',', ' ' ) ); ?>
                                            </td>
                                            <td class="invoice-details__cell total">
                                                <?php echo number_format( $item_total, 2, ',', ' ' ); ?>
                                            </td>
                                        </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                        <div class="invoice-compilation">
                            <table>
                                <tbody>
                                    <?php foreach ($items_vat as $vat => $value) : ?>
                                    <tr>
                                        <td>Netto <?php echo $vat; ?>%</td>
                                        <td><?php echo number_format( $value['netto'], 2, ',', ' ' ); ?> SEK</td>
                                    </tr>
                                    <tr>
                                        <td>Moms <?php echo $vat; ?>%</td>
                                        <td><?php echo number_format( $value['vat'], 2, ',', ' ' ); ?> SEK</td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td>Brutto</td>
                                        <td><?php echo $invoice_total; ?> SEK</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Att betala</td>
                                        <td><?php echo $invoice_total; ?> SEK</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </section>

                    <footer class="invoice__footer">
                        <div>
                            <h4>Adress</h4>
                            <address>
                                <?php echo $options['name']; ?><br />
                                <?php echo nl2br( $options['address'] ); ?>
                            </address>
                        </div>
                        <div>
                            <h4>E-post</h4>
                            <?php echo $options['email']; ?>
                        </div>
                        <div>
                            <h4>Org.nr.</h4>
                            <?php echo $options['org_number']; ?>
                            <?php if ( '1' === $options['f_approved'] ): ?>
                                <br />Godkänd för F-skatt
                            <?php endif ?>
                        </div>
                        <div>
                            <h4>Webbplats</h4>
                            <?php echo $options['homepage']; ?>
                        </div>
                    </footer>
                </article>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_footer(); ?>
    </body>
</html>
