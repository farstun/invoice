<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://farstun.com
 * @since      1.0.0
 *
 * @package    Invoice
 * @subpackage Invoice/admin
 */

 define( 'ACCOUNT_TYPES', array(
	'Bankgiro',
	'Postgiro',
	'Bankkonto'
 ) );

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Invoice
 * @subpackage Invoice/admin
 * @author     Jonas Eriksson <jonas@farstun.com>
 */
class Invoice_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The top level menu page slug of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $settings_page    The settings page of this plugin.
	 */
	private $menu_page_slug;

	/**
	 * The settings page of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $settings_page    The settings page of this plugin.
	 */
	private $settings_page;

	/**
	 * The option name of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $option_name    The option name of this plugin.
	 */
	private $option_name;

	/**
	 * Holds the values to be used in the fields callbacks.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string[]  $options    The options for this plugin.
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string    $plugin_name       The name of this plugin.
	 * @param    string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $settings_page, $options_name ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->menu_page_slug = $plugin_name . '.php';
		$this->settings_page = $settings_page;
		$this->option_name = $options_name;
	}

	/**
	 * Register the stylesheets for the custom post types.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_cpt_styles() {
		if ( is_singular( array( 'farstun_invoice' ) ) ) {
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/farstun-invoice.css', array(), $this->version, 'all' );
		}
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/invoice-admin.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		// Get data to make available for js
		$this->options = get_option( $this->option_name );
		$data = array(
			'due_days' => $this->options['due_days']
		);

		wp_enqueue_media();
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/invoice-admin.js', array( 'jquery', 'acf-input' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'invoice_options', $data);
	}

	/**
	 * Creates the Settings link within the Plugins page.
	 *
	 * @since  1.0.0
	 * @access public
	 *
	 * @param array  $links Links associated with the plugin.
	 * @param string $file  The plugin filename.
	 *
	 * @return array $links Links associated with the plugin, after the Settings link is added.
	 */
	public function add_settings_link( $links, $file ) {
		if ( $file !== INVOICE_PLUGIN_BASE ) {
			return $links;
		}

		array_unshift( $links, '<a href="' . esc_url( admin_url( 'admin.php' ) ) . '?page=' . $this->settings_page . '">Settings</a>' );

		return $links;
	}

	public function add_invoice_menu() {
		add_menu_page( 
			'Farstun Invoice', 
			'Farstun Invoice', 
			'manage_options',
			$this->menu_page_slug,
			'',
			'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTkiIGhlaWdodD0iODciIHZpZXdCb3g9IjAgMCA5OSA4NyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTMzLjA2MyA1MS41MDcxTDM0LjQyNDYgNTEuNTE3MkM0Mi42OTUgNTEuNTc4NSA1MC42MDEyIDU1LjA1NzIgNTYuNDA0MSA2MS4xODhDNjIuMjA3IDY3LjMxODcgNjUuNDMxMSA3NS41OTkzIDY1LjM2NzIgODQuMjA4MUw2NS4zNTI4IDg2LjE1MzJMMzIuODA3NyA4NS45MTE3TDMzLjA2MyA1MS41MDcxWiIgZmlsbD0iYmxhY2siLz4KPHBhdGggZD0iTTMyLjg5NjcgNTEuMzIxOEMzMi45MTU0IDQ2Ljc5NzcgMzQuNjU5OSA0Mi40NjYxIDM3Ljc0NjMgMzkuMjc5OEM0MC44MzI4IDM2LjA5MzUgNDUuMDA4NCAzNC4zMTM2IDQ5LjM1NDYgMzQuMzMxNkM1My43MDA4IDM0LjM0OTUgNTcuODYxNiAzNi4xNjQgNjAuOTIxNiAzOS4zNzU3QzYzLjk4MTYgNDIuNTg3NCA2NS42OTAyIDQ2LjkzMzMgNjUuNjcxNSA1MS40NTc0TDk4LjM0NjkgNTEuNTkyNUM5OC40NjM4IDIzLjMyMzIgNzYuNTQzMiAwLjMxNTcxNCA0OS4zODUzIDAuMjAzMzg2QzIyLjIyNzQgMC4wOTEwNTgyIDAuMTE3MTI5IDIyLjkxNjQgMC4wMDAyMDQ2NzIgNTEuMTg1OCIgZmlsbD0iYmxhY2siLz4KPC9zdmc+Cg==',
			//'dashicons-bank',
			20
		);
	}

	public function remove_bulk_actions( $actions ) {
		// Remove all actions
		return array();
	}

	public function remove_date_filter( $months, $post_type ) {
		if ( 'farstun_customer' === $post_type ) {
			// Remove all months
			return array();
		}

		return $months;
	}

	public function remove_quick_edit( $actions, $post ) {
		if ( 'farstun_invoice' === $post->post_type || 'farstun_customer' === $post->post_type ) {
			unset($actions['inline hide-if-no-js']);
		}

     	return $actions;
	}

	public function add_email_link( $actions, $post ) {
		if ( 'farstun_invoice' === $post->post_type ) {
			$customer = get_field('customer');
			$email = get_field( 'email', $customer->ID );
			$number = get_the_title( $post->ID );
			$options = get_option( 'invoice_option' );

			$subject = 'Faktura #' . $number . ' från ' . $options['name'];
			$body = 'Hej,%0D%0A%0D%0AHär är din faktura.%0D%0A%0D%0A%0D%0AMed vänlig hälsning,%0D%0AFarstun AB';

			$actions['email'] = '<a href="mailto:' . $email . '?subject=' . $subject . '&body=' . $body . '">Email</a>';
		}

     	return $actions;
	}

	/**
	 * Register custom post types.
	 *
	 * @since  1.0.0
	 * @access public
	 */
	public function register_post_types() {
		register_post_type('farstun_invoice',
			array(
				'labels' => array(
					'name'               => 'Invoices',
					'singular_name'      => 'Invoice',
					'add_new'            => 'Add New Invoice',
					'add_new_item'       => 'Add New Invoice',
					'new_item'           => 'New Invoice',
					'edit_item'          => 'Edit Invoice',
					'view_item'          => 'View Invoice',
					'all_items'          => 'Invoices',
					'search_items'       => 'Search Invoices',
					'not_found'          => 'No invoices found.',
					'not_found_in_trash' => 'No invoices found in Trash.',
				),
				'public'    => true,
				'exclude_from_search' => true,
				'show_in_menu' => $this->menu_page_slug,
				'supports'  => array( 'title' ), // Uses ACF Field Group
				'rewrite' => array(
					'slug' => 'farstun-invoice'
				),
			)
		);

		register_post_type('farstun_customer',
			array(
				'labels' => array(
					'name'               => 'Customers',
					'singular_name'      => 'Customer',
					'add_new'            => 'Add New Customer',
					'add_new_item'       => 'Add New Customer',
					'new_item'           => 'New Customer',
					'edit_item'          => 'Edit Customer',
					'view_item'          => 'View Customer',
					'all_items'          => 'Customers',
					'search_items'       => 'Search Customers',
					'not_found'          => 'No customers found.',
					'not_found_in_trash' => 'No customers found in Trash.',
				),
				'public'    => false,
				'show_ui' => true,
				'show_in_menu' => $this->menu_page_slug,
				'supports'  => array( 'title' ), // Uses ACF Field Group
				'rewrite' => array(
					'slug' => 'farstun-customer'
				),
			)
		);
	}

	public function register_custom_acf_fields() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf.php';
	}

	// https://developer.wordpress.org/reference/hooks/update_plugins_hostname/
	public function check_for_updates( $update, $plugin_data, $plugin_file ) {
		if ( empty( $plugin_data['UpdateURI'] ) || ! empty( $update ) ) {
			return $update;
		}
		
		$response = wp_remote_get( $plugin_data['UpdateURI'] );
		
		if ( empty( $response['body'] ) ) {
			return $update;
		}
		
		$custom_plugins_data = json_decode( $response['body'], true );
		
		if ( ! empty( $custom_plugins_data[ $plugin_file ] ) ) {
			return $custom_plugins_data[ $plugin_file ];
		}

		return $update;	
	}

	public function set_custom_edit_customer_columns( $columns ) {
		// Remove checkbox and date (Published) columns
		unset( $columns['cb'] );
		unset( $columns['date'] );

		$columns['customer_id'] = 'Customer Id';
		$columns['email'] = 'Email';
		$columns['organization_number'] = 'Organization Number';

		return $columns;
	}

	public function custom_customer_column( $column, $post_id ) {
		switch ( $column ) {	
			case 'customer_id' :
				echo $post_id;
				break;

			default:
				the_field($column, $post_id);
		}
	}

	public function set_customer_sortable_columns( $columns ) {
		$columns['customer_id'] = 'customer_id';
		$columns['email'] = 'email';
		$columns['organization_number'] = 'organization_number';

		return $columns;
	}

	public function set_custom_edit_invoice_columns( $columns ) {
		// Remove checkbox and date (Published) columns
		unset( $columns['cb'] );
		unset( $columns['date'] );

		$columns['title'] = 'Number';
		$columns['state'] = 'State';
		$columns['customer_name'] = 'Customer';
		$columns['total_amount'] = 'Amount (incl VAT)';
		$columns['invoice_date'] = 'Invoice Date';
		$columns['due_date'] = 'Due Date';

		return $columns;
	}

	public function add_top_link( $views ) {
		$attributes = 'class=""';
		if ( filter_input(INPUT_GET, 'state') === 'unpaid' ) {
			$attributes ='class="current" aria-current="page"';
		}

		$args = array(
			'post_type' => 'farstun_invoice',
			'post_per_page' => -1, // get ALL posts
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'state',
					'compare' => 'LIKE',
					'value' => 'unpaid'
				)
			)
		);
		$query = new WP_Query($args);
		$post_count = count($query->posts);

		array_push(
			$views,
			sprintf(
			  '<a href="%1$s" %2$s>%3$s <span class="count">(%4$s)</span></a>',
			  add_query_arg(
				array(
				  'post_type'   => 'farstun_invoice',
				  'state' => 'unpaid',
				),
				'edit.php'
			  ),
			  $attributes,
			  __( 'Unpaid' ),
			  $post_count
			)
		);

		return $views;
	}

	public function add_invoice_total( $which ) {
		if ( 'farstun_invoice' === filter_input( INPUT_GET, 'post_type' ) && 'top' === $which ) {
			global $wp_query;
			
			$invoice_total = array_reduce($wp_query->posts, function($total, $post) {
				return $total + get_post_meta( $post->ID, 'total_amount', true );
			}, 0);

			printf(
				'<div class="tablenav-extra"><strong>%s SEK</strong></div>',
				number_format($invoice_total , 2, ',', ' ')
			);
		}
	}

	public function custom_invoice_column( $column, $post_id ) {
		switch ( $column ) {	
			case 'customer_name':
				echo get_post_meta( $post_id, 'customer_name', true );
				break;

			case 'state' :
				$state = get_field('state', $post_id);

				if ( $state === 'unpaid' ) {
					$due_date = get_field('due_date', $post_id);
					if ( !empty( $due_date ) ) {
						$date = DateTime::createFromFormat('Y-m-d', $due_date);
						$now = new DateTime();
						if ( $date < $now) {
							$state = 'overdue';
						}
					}
				}

				echo '<strong class="state-' . $state . '">' . ucfirst($state) . '</strong>';
				break;

			case 'total_amount' :
					$invoice_total = get_post_meta( $post_id, 'total_amount', true );
					$invoice_total = empty( $invoice_total ) ? 0 : $invoice_total;
					printf('%s SEK', number_format($invoice_total , 2, ',', ' '));
					break;

			default:
				the_field($column, $post_id);
		}
	}

	public function set_invoice_sortable_columns( $columns ) {
		$columns['state'] = 'state';
		$columns['title'] = 'title';
		$columns['customer_name'] = 'customer_name';
		$columns['total_amount'] = 'total_amount';
		$columns['invoice_date'] = 'invoice_date';
		$columns['due_date'] = 'due_date';

		return $columns;
	}

	public function custom_orderby( $query ) {
		$orderby = $query->get( 'orderby');
		$post_type = $query->get( 'post_type');

		if ( 'farstun_invoice' === $post_type && 'unpaid' === filter_input(INPUT_GET, 'state') ) {
			$query->set( 'meta_query', array(
				array(
					'key' => 'state',
					'compare' => 'LIKE',
					'value' => 'unpaid'
				)
			));
		}

		if ( $orderby ) {
			if ( 'farstun_invoice' === $post_type &&
				in_array( $orderby, [ 'state', 'customer_name', 'invoice_date', 'due_date' ] ) ) {
				$query->set( 'meta_key', $orderby );
				$query->set( 'orderby', 'meta_value' );
			}

			else if ( 'farstun_invoice' === $post_type && 'total_amount' === $orderby ) {
				$query->set( 'meta_key', $orderby );
				$query->set( 'orderby', 'meta_value_num' );
			}

			else if ( 'farstun_customer' === $post_type &&
				in_array( $orderby, [ 'email', 'organization_number' ] ) ) {
				$query->set( 'meta_key', $orderby );
				$query->set( 'orderby', 'meta_value' );
			}
		}
		 else if ( $query->is_admin ) {
			if ( 'farstun_customer' === $post_type ) {
				$query->set('orderby', 'title');
				$query->set('order', 'ASC');
			}
		}

		return $query;
	}

	public function missing_plugin_error() {
		?>
		<div class="notice notice-error">
			<p>
				The <a href="https://www.advancedcustomfields.com/pro">Advanced Custom Fields PRO</a>
				plugin is required by Farstun Invoice.
			</p>
		</div>
		<?php
	}

	public function check_required_plugins() {
		if ( current_user_can( 'activate_plugins' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) )
				add_action( 'admin_notices', array( $this, 'missing_plugin_error' ) );
		}
	}

	// Calculate total invoice amount including VAT
	protected function calculate_total_amount( $post_id ) {
		$items = get_field('items');
		if ( empty( $items ) ) {
			return 0;
		}

		// Create array of all item price with and without VAT, grouped by VAT
		$items_vat = [
			'0' => ['netto' => 0, 'vat' => 0],
			'6' => ['netto' => 0, 'vat' => 0],
			'12' => ['netto' => 0, 'vat' => 0],
			'25' => ['netto' => 0, 'vat' => 0],
		];
		foreach ($items as $item) {
			$item_netto_total = (int) $item['number'] * (int) $item['price'];
			$items_vat[$item['vat']]['netto'] += $item_netto_total;
			$items_vat[$item['vat']]['vat'] += $item_netto_total * (int) $item['vat'] * 0.01;;
		}
		$items_vat = array_filter($items_vat, function($value) {
			return $value['netto'] > 0;
		});
		
		return array_reduce($items_vat, function($result, $value) {
			return $result + $value['netto'] + $value['vat'];
		}, 0);
	}

	/**
	 * Update invoice number when invoice is saved.
	 * Using post meta to prevent multiple executions for same invoice.
	 * 
	 * If there is no invoice number, use current from settings and update it.
	 *
	 * @since  1.0.0
	 * @access public
	 *
	 * @param int|string $post_id  The ID of the post being edited.
	 */
	public function update_invoice_fields( $post_id ) {
		$post = get_post( $post_id );
		if ( $post && $post->post_type === 'farstun_invoice' ) {
			$invoice_number = get_field('invoice_number', $post_id);

			if ( empty( get_post_meta( $post_id, 'check_if_run_once' ) ) ) {
				// Only update if there is no current number
				if ( empty( $invoice_number ) ) {
					$this->options = get_option( $this->option_name );

					// If there is an invoice number in settings
					if ( is_array( $this->options ) && isset( $this->options['invoice_number'] )) {
						$invoice_number = intval($this->options['invoice_number']);

						// Update invoice
						update_field('invoice_number', $invoice_number, $post_id);

						// Increase settings
						$this->options['invoice_number'] = $invoice_number + 1;
						update_option( $this->option_name, $this->options );
					}
				}

				// Update the meta so it won't run again
				update_post_meta( $post_id, 'check_if_run_once', true );
			}

			$customer = get_field('customer', $post_id);
			update_post_meta( $post_id, 'customer_name', $customer ? $customer->post_title : 'n/a' );

			update_post_meta( $post_id, 'total_amount', $this->calculate_total_amount( $post_id ) );

			// Ensure title matches invoice number
			$post->post_title = $invoice_number;
			$post->post_name = $invoice_number;
			wp_update_post($post);
			flush_rewrite_rules();
		}
	}

	/**
	 * Hide "title" field on invoice edit page
	 */
	public function hide_invoice_title() {
		remove_post_type_support( 'farstun_invoice', 'title' );
	}

	/**
	 * Prevent access to Customer and Invoice post types for unauthorized users.
	 *
	 * @since  1.0.0
	 * @access public
	 */
	public function redirect_unauthenticated() {
		global $post;

		if ( $post && ( $post->post_type === 'farstun_customer' || $post->post_type === 'farstun_invoice' ) && ! is_user_logged_in() ) {
			wp_redirect( home_url() ); 
			exit();
		}
	}

	/**
	 * Load plugin template for single Invoice post.
	 * Only use plugin templates if matching template is not found in theme or child theme directories
	 *
	 * @since  1.0.0
	 * @access public
	 */
	public function load_invoice_template( $template ) {
		global $post;

		if ( 'farstun_invoice' === $post->post_type &&
			locate_template( array( 'single-farstun_invoice.php' ) ) !== $template ) {
			show_admin_bar(false);
			return plugin_dir_path( __FILE__ ) . 'templates/single-farstun_invoice.php';
		}

		return $template;
	}
}
